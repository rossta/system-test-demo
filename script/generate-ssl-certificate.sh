#!/usr/bin/env bash

if [[ ! -n "$1" ]]; then
  echo "Argument 1 should be the domain name" 1>&2
  exit 1
fi

DOMAIN=$1
mkdir -p config/ssl

cat > openssl-$DOMAIN.cnf <<-EOF
  [req]
  distinguished_name = req_distinguished_name
  x509_extensions = v3_req
  prompt = no
  [req_distinguished_name]
  CN = *.$DOMAIN
  [v3_req]
  keyUsage = keyEncipherment, dataEncipherment
  extendedKeyUsage = serverAuth
  subjectAltName = @alt_names
  [alt_names]
  DNS.1 = *.$DOMAIN
  DNS.2 = $DOMAIN
EOF

openssl req \
  -new \
  -newkey rsa:2048 \
  -sha1 \
  -days 3650 \
  -nodes \
  -x509 \
  -keyout config/ssl/$DOMAIN.key \
  -out config/ssl/$DOMAIN.crt \
  -config openssl-$DOMAIN.cnf

# rm openssl-$HOST.cnf
